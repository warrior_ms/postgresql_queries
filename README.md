## Submitted By : Mustafa Sadriwala

## Assignment : Queries


This repository contains a file which lists down the queries along with the sample outputs given as the assignment to us by Mr. Sarang.
Questions:

1. List of all consultants having more than one submission.

2. List all the interviews of consultants.

3. List all Purchase orders of marketters.

4. Unique vendor company names for which client location is the same.

5. Count of consultants who are submitted in the same city.

6. Name of consultants and clients who have been submitted by same vendor.